//
//  TopicTableViewCell.swift
//  BasicUITextField
//
//  Created by Arthit Thongpan on 3/4/17.
//  Copyright © 2017 Arthit Thongpan. All rights reserved.
//

import UIKit

class TopicTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    var topic: Topic! {
        didSet {
            titleLabel.text = "หัวข้อ: \(topic.title)"
            detailLabel.text = "รายละเอียด: \n\n\(topic.detail)"
            nameLabel.text = "โดย: \(topic.name)"
        }
    }
}

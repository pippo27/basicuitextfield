//
//  AppDelegate.swift
//  BasicUITextField
//
//  Created by Arthit Thongpan on 3/2/17.
//  Copyright © 2017 Arthit Thongpan. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }
}

